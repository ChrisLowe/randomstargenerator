package com.lowware.game.spacecommander;

import java.util.*;
import java.text.DecimalFormat;

public class Planet {

	private static final String[] PLANET_TYPES = {
		"Super Hot Gas Giant",
		"Hot Gas Giant",
		"Hot Rocky",
		"Inferior Rocky",
		"Superior Rocky",
		"Outer Rocky",
		"Gas Giant" };
		
	private static final int TYPE_SUPERHOTGAS = 0;
	private static final int TYPE_HOTGAS = 1;
	private static final int TYPE_INFERIOR = 2;
	private static final int TYPE_SUPERIOR = 3;
	private static final int TYPE_OUTER = 4;
	private static final int TYPE_GIANT = 5;


	private String name;
	private LinkedList<Satellite> satellites;
	private int numSatellites;
	private String systemName;
	private int position;
	
	private int type;
	private double mass; //mutiple of earth mass
	private double radius; //multiple of AU
	
	
	
	public String getType() {
	
		//No idea why it was doing this ... very strange
		if ((radius > 0.25) && (type == 0)) {
			type = 2;
			return "Inferior Rocky";
		}
	
		switch (type) {
			case 0:
				return "Superhot Gas Giant";
			case 1:
				return "Hot Gas Giant";
			case 2:
				return "Inferior Rocky";
			case 3:
				return "Superior Rocky";
			case 4:
				return "Outer Rocky";
			case 5:
				return "Gas Giant";		
			default:
				return "";
		}
	}
	
	
	private void setType() {
		Random r = new Random();
	
		if (this.radius < 0.25) {
			int rand = r.nextInt(3);
			if (rand == 0) {
				this.type = TYPE_SUPERHOTGAS;
			
				setMass(0);
			} else {
				this.type = TYPE_INFERIOR;
				
				setMass(2);
			}
		} else {
		
			if (this.radius < 0.75) {
				int rand = r.nextInt(1);
				if (rand == 0) {
					this.type = TYPE_HOTGAS;
					setMass(1);
				} else {
					this.type = TYPE_INFERIOR;
					setMass(2);
				}
			} else {
		
				if (this.radius > 3.5) {
					int ra = r.nextInt(1);
					if (ra == 0) {
						this.type = TYPE_OUTER;
						setMass(4);
					} else {
						this.type = TYPE_GIANT;
						setMass(5);
					}			
				}
			}
		
		if ((this.radius > 0.75) && (this.radius < 1.75)) {
			this.type = TYPE_SUPERIOR;
			setMass(3);
		}
	
		}
	}
	
	private void setMass(int type) {
	
	}
	
	
	private void setRadius() {
	
		Random r = new Random();
		int rand = r.nextInt(100);
		double rad = 0.025 + (0.05 * rand);
		for (int i = 0; i < position + 1; i++) {
			rand = r.nextInt(100);
			rad += (rand * i * 0.5);			
		}
			
		this.radius = rad / 75;
	
		if (this.radius > 5) {
			rand = r.nextInt(20);
			this.radius = this.radius * rand;	
			if (this.radius > 100) {
				this.radius = this.radius / 2;
			}
		}
		
		if (this.radius < 0.09) {
			this.radius = this.radius * 14;
		}
	}
	
	private String getRadius() {
		DecimalFormat df = new DecimalFormat("#.###");
		String rad = df.format(this.radius);
		return rad + " AU";
		
		
	}


	private void generateSatellites() {
		Random r = new Random();
		int temp = r.nextInt(10);
		if (temp > 4) {		
			numSatellites = r.nextInt(11) % 8 + 1;
			for (int i = 0; i < numSatellites; i++) {
				satellites.add(new Satellite());
			}
		}
	}
	
	private String getSatellites() {
		StringBuffer b = new StringBuffer();
		Iterator satIterator = satellites.iterator();
		while (satIterator.hasNext()) {
			b.append(satIterator.next().toString());
			b.append("\n");
		}
		
		return b.toString();
	}
	
	private void setName() {
		StringBuffer b = new StringBuffer();
		b.append(this.systemName);
		b.append(" ");
		
		switch (position) {
			case 0: 
				b.append("I");
				break;
			case 1:
				b.append("II");
				break;
			case 2:
				b.append("III");
				break;
			case 3:
				b.append("IV");
				break;
			case 4:
				b.append("V");
				break;
			case 5:
				b.append("VI");
				break;
			case 6:
				b.append("VII");
				break;
			case 7:
				b.append("VIII");
				break;
			case 8:
				b.append("XI");
				break;
			case 9:
				b.append("X");
				break;
			default:
				break;
		}		
		
		this.name = b.toString();
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append(this.name);
		b.append("\n");
		b.append("Orbit: " + getRadius() + "\n");
		b.append("Type: " + getType() + "\n");
		b.append("This planet has " + numSatellites + " satellites\n");
		return b.toString();		
	}

	public Planet(String systemName, int position) {
		this.systemName = systemName;
		this.position = position;
		setName();
		setRadius();
		setType();
		satellites = new LinkedList<Satellite>();
		generateSatellites();
		
	}

}