
Title: Random star system generator

Author: Christopher Lowe
	lowey2002@gmail.com


Abstract:  This is a content creation system I wrote one afternoon
to generate random star systems. The content is for a game I was
designing called 'Space Commander'. The user is given a spaceship
that is able to travel faster than light but has no control over
it's destination other than to return to a visited system. The goal
of the game is to explore the galaxy for habitable planets and set up
colonies. 

I wanted to randomly generate believable star systems that you
would likely find in the galaxy. To do this I first researched the
properties of the various star types and stellar categories and used
both our own solar system and the results of the Kepler project
to model the randomly cascading setters that generate the data.


Sample output:

Naos Theta
Single Star System
Type: Type G Main Sequence
Temperature: 4600K
Mass: 1.42 solar masses

There are 2 planets in this system
Naos Theta I
Orbit: 0.238 AU
Type: Inferior Rocky
This planet has 0 satellites

Naos Theta II
Orbit: 1.115 AU
Type: Superior Rocky
This planet has 2 satellites




 



