package com.lowware.game.spacecommander;

import java.util.Random;
import java.text.DecimalFormat;

public class Star
{

	private static int TYPE_O = 0;  //0.00001%
	private static int TYPE_B = 1;  //0.1%
	private static int TYPE_A = 2;  //0.7%
	private static int TYPE_F = 3;  //2%
	private static int TYPE_G = 4;  //3.5%
	private static int TYPE_K = 5;  //8%
	private static int TYPE_M = 6;  //80%    //used something more interesting than statistical correctness
	
	private static int MAIN_SEQUENCE = 0;
	private static int DWARF = 1;		//5%
	private static int GIANT = 2; 		//0.4%
	private static int SUPERGIANT = 3; //0.0001%

	private int type;
	private int age;
	private int temperature; //in Kelvin (K)
	private double mass; //multiple of sol
	private double radius; //multiple of sol

		
	
	
	private Random r;
	
	
	
	
	
	
	public String getTemperature() {
		int n = temperature;
		n = (n + 50) / 100 * 100;
	
		return Integer.toString(n) + "K";
	}
	
	public String getAge() {
		switch (this.age) {
			case 0:
				return "Main Sequence";
			case 1:
				return "Dwarf";
			case 2:
				return "Giant";
			case 3:
				return "SuperGiant";
			default:
				return "Unknown";
		}
	}
	
	public String getMass() {
		DecimalFormat df = new DecimalFormat("#.##");
		String dmas = df.format(this.mass);
        return dmas + " solar masses";
	}
	
	public String getType() {
		switch (this.type) {
			case 0:
				return "Type O";
			case 1:
				return "Type B";
			case 2:
				return "Type A";
			case 3:
				return "Type F";
			case 4:
				return "Type G";
			case 5:
				return "Type K";
			case 6:
				return "Type M";
			default:
				return "Unknown type";		
		}
	}
	
	
	private void setMass() {
		int rand =0;
		
		if ((this.type == TYPE_O) || (this.type == TYPE_B)) {
			this.mass = r.nextDouble() + r.nextInt(40) + 10;
		} else if (this.type == TYPE_K) {
			this.mass = r.nextDouble();
		} else {
			this.mass = 2 + r.nextDouble() - 1;		
		}
		
		if (this.age == DWARF) {
			this.mass = this.mass / 20;
		}
		
		if (this.age == SUPERGIANT) {
			this.mass = this.mass * 15;
		} else if (this.age == GIANT) {
			this.mass = this.mass * 8;
		}
		
		if (this.mass < 0.075) {
			this.mass =  this.mass * (r.nextInt(40));
		}
		
	}
	
	
	private void setTemperature() {
		int rand = 0; 
		
		if (this.age == DWARF) {
			rand = r.nextInt(1500) - 750;
			this.temperature = 4000 + rand;
			
		} else if (this.age == GIANT) {
			rand = r.nextInt(6000) - 3000;
			this.temperature = 6000 + rand;
			
		} else if (this.age == SUPERGIANT) {
			rand = r.nextInt(15000) - 7500;
			this.temperature = 20000 + rand;
			
		} else {
			rand = r.nextInt(2000) - 1000;
			this.temperature = 5000 + rand;
		}		
	
		if ((this.type == TYPE_O) || (this.type == TYPE_B)) {
			this.temperature = this.temperature + (r.nextInt(2000) + 1500);
		}
	
	}
	
	
	private void setAge() {
		int a = r.nextInt(30);
		if (a < 2) {
			this.age = SUPERGIANT;
		} else if (a < 7) {
			this.age = GIANT;
		} else if (a < 16) {
			this.age = DWARF;
		} else {
			this.age = MAIN_SEQUENCE;
		}
	}
	
	private void setType() {
		int t = r.nextInt(60);
		if (t < 2) {
			this.type = TYPE_O;
		} else if (t < 4) {
			this.type = TYPE_B;
		} else if (t < 8) {
			this.type = TYPE_A;
		} else if (t < 13) {
			this.type = TYPE_F;
		} else if (t < 24) {
			this.type = TYPE_G;
		} else if (t < 37) {
			this.type = TYPE_K;
		} else {
			this.type = TYPE_M;
		}		
	}


	private void setName() {
		
	}


	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("Type: " + getType() + " " + getAge() + "\n");
		b.append("Temperature: " + getTemperature() + "\n");
		b.append("Mass: " + getMass() + "\n");
		
		return b.toString();
	}


	public Star() {
		r = new Random();
		setType();
		setAge();
		setTemperature();
		setMass();
	}

}