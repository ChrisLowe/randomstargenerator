package com.lowware.game.spacecommander;

import java.util.*;


public class StarSystem {


	private static final String[] STAR_NAME_PREFIX = { "Homam", "Aurigae", "Avior", "Giedi", "Alnairor", "Asellus", "Alhena", "Pherkad", "KappaOrionis", "Eridani", "Thuban", "Alkalurops", "Algedi", "Alcyone", "Mizar", "Menkar", "Zosma", "Wasat", "Alfirk", "Nin", "Merope", "Sagittarii", "Yed", "Atlas", "Atria", "Izar", "Rastaban", "Pulcherrima", "Algiebaor", "Zubeneschamali", "Kent", "Cygni", "Gemma", "Aquarii", "Kaus", "Pavonis", "Almach", "Almaak", "Equulei", "PiscisAustrini", "Arneb", "Mirfakor", "Arkab", "Leonis", "Dabih", "Velorum", "Tarazed", "Alkaid", "Geminorum", "Merak", "Nihal", "Enif", "Nunki", "Majoris", "Antares", "Alioth", "Pegasi", "Sheratan", "Becrux", "Spica", "Capella", "Mintaka", "Lesath", "Propus", "Sheliak", "Mesarthimor", "Zaurak", "Ursae", "Deneb", "Mothallah", "Sadr", "Denebola", "Arietis", "Coronae", "Draconis", "Muhlifain", "KentaurusorRigil", "Achernar", "Hydrae", "Rukbat", "Bootis", "Diphda", "Kochab", "Rasalhague", "Electra", "Maia", "Eltanin", "Mira", "Porrima", "Menkalinan", "Rigel", "Mirphak", "Suhail", "Yildun", "Centauri", "Naos", "MuBootis", "Toliman", "Gienah", "Capricorni", "Venaticorum", "Castor", "Gruis", "Markab", "Alrescha", "Canum", "Canis", "AsellusBorealis", "Kornephoros", "Herculis", "Sirrah", "Cassiopeiae", "Sirius", "DenebAlgedi", "UrsaeMajoris", "Regulus", "CanisMajoris", "Tauri", "Leporis", "Phact", "Persei", "DenebKaitos", "Alnitak", "Algeiba", "Algenib", "Chara", "Australis", "Acrab", "Acrux", "Lyrae", "Sadachbia", "Sadalmelik", "Canopus", "Alshain", "Virginis", "Errai", "Albireo", "Dschubba", "Polaris", "Phoenicis", "Zavijava", "Vega", "Posterior", "Cancri", "Trianguli", "Pollux", "Sadalsuud", "Nashira", "Miaplacidus", "Menkent", "Crucis", "Kocabor", "Columbae", "Arcturts", "Seginus", "Fomalhaut", "Alnilam", "Adhara", "Benetnasch", "Alpheccaor", "Asterope", "Kitalpha", "Nekkar", "Minoris", "Alphekka", "Graffias", "Librae", "Taygeta", "Zubenelgenubi", "Mesartim", "Borealis", "Ruchbah", "Carinae", "Cebalrai", "Ankaa", "Celaeno", "Alnath", "Alnasl", "Alnair", "Dubhe", "Prior", "Peacock", "Gomeisa", "Serpentis", "Ophiuchi", "Mimosa", "Aquilae", "Aldebaran", "Megrez", "CorCaroli", "Vindemiatrix", "Alcor", "Acamar", "Almak", "Alpheratz", "Scheat", "Algol", "Saiph", "Gacrux", "Puppis", "PhadorPhecda", "Corvi", "Sabik", "Alderamin", "Elnath", "Hamal", "Alrami", "Muliphein", "Caph", "Upsilon", "Cephei", "Ceti", "Andromedae", "Acubens", "Hadar", "Scorpii", "Rasalgethi", "Mirzam", "Cursa", "Pleione", "Bellatrix", "Procyon", "Girtab", "Mirach", "Alya", "Lambda", "Orionis", "Altair", "Betelgeuse", "Unukalhai", "Shaula", "Wezen", "Agena", "Piscium" }; 
	private static final String[] STAR_NAME_SUFFIX = { "Alpha", "Beta", "Gamma", "Delta", "Epsilon", "Zeta", "Theta", "Iota", "Kappa", "Lambda", "Mu", "Xi", "Omicron", "Phi", "Rho", "Sigma", "Tau", "Upsilon", "Psi", "Omega" };
	


	private String name;
	private LinkedList<Star> stars;
	private LinkedList<Planet> planets;
	private int numStars;
	private int numPlanets;



	
	private void setName() {
		Random r = new Random();
		StringBuffer b = new StringBuffer();
		int ran = r.nextInt(STAR_NAME_PREFIX.length);
		b.append(STAR_NAME_PREFIX[ran]);
		b.append(" ");
		ran = r.nextInt(STAR_NAME_SUFFIX.length);
		b.append(STAR_NAME_SUFFIX[ran]);
		this.name = b.toString();		
	}
	
	public String getName() {
		return this.name;
	}


	private void generateSystem() {
	
		for (int i = 0; i < numStars; i++) {
			stars.add(new Star());
		}
				
		for (int i = 0; i < numPlanets; i++) {
			Planet p = new Planet(getName(), i);
			planets.add(p);
		}
	}


	public String toString() {
		Iterator starIterator = stars.iterator();
		Iterator planetIterator = planets.iterator();
		StringBuffer b = new StringBuffer();
		
		b.append(this.name);
		b.append("\n");
		
		if (numStars == 1) {
			b.append("Single Star System\n");
		} else if (numStars == 2) {
			b.append("Binary Star System\n");
		} else if (numStars == 3) {
			b.append("Trinary Star System\n");
		} else {
			b.append("Complex co-orbital System\n");
		}
		
		while (starIterator.hasNext()) {
			b.append(starIterator.next().toString());
			b.append("\n");
		}
		
		b.append("There are " + numPlanets + " planets in this system\n");
		while (planetIterator.hasNext()) {
			Planet p = (Planet) planetIterator.next();
			b.append(p.toString());
			b.append("\n");			
		}
		
		return b.toString();		
	}


	public StarSystem() {
		Random r = new Random();
		
		int rand = r.nextInt(10);
		if (rand < 7) {
			numStars = r.nextInt(2) + 1;
		} else {
			numStars = r.nextInt(3) + 1;
		}		
		
		numPlanets = (r.nextInt(12) % 9) + 1;
		
		setName();
		
		stars = new LinkedList<Star>();
		planets = new LinkedList<Planet>();

		generateSystem();
		
		System.out.println(this.toString());
	}
	


}